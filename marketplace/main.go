package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rwirdemann/bffdashboard/marketplace/usecase"
	"net/http"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/login", usecase.LoginHandler).Methods("POST")
	router.HandleFunc("/seller/{id}", usecase.SellerHandler).Methods("GET")
	http.Handle("/", router)
	fmt.Printf("Marketplace server started on http://localhost:8080...")
	http.ListenAndServe(":8080", router)
}

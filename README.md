# BBFDashboard

The goal of this project is to collect and build simplified versions of business fitness functions that we developed and experienced with in real world projects. 

## Marketplace

The marketplace forms the sample application that is supervised by the business fitness functions provided in this project.

```
go run marketplace/main.go
```

## Dashboard

The dashboard visualizes all installed business fitness functions as well as their current status.

```
go run dashboard/main.go
```

## Business Fitness Functions

### Authentication

Supervises the authentication mechanism of the marketplace implemented by JWT validation provided as authentication in HTTP requests. 

Reasons to fail:
- identity provider changes its public key without notifying its clients

How to reproduce:
- change the public key contained in .env. The key will picked up on the fly, thus no restart of any service is required.
- check the dashboard on localhost:8082 that updates itself automatically every 5 seconds
- change the key back to the correct one and check back the dashboard to verify the error has been resolved


